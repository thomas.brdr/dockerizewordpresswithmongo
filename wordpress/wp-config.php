<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp' );

/** Database username */
define( 'DB_USER', 'wp' );

/** Database password */
define( 'DB_PASSWORD', 'secret' );

/** Database hostname */
define( 'DB_HOST', 'mysql' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '@@FeZEq!3oIc_`U*9mZ!Q[~IB_aJPx]a8)m-9xFGpdV!S,*b.JB^L$,MrgLxT Td' );
define( 'SECURE_AUTH_KEY',  '8Ig!FFQ5lWt@b)-k1 3O(&=7EjIL69>H$bFrcZm#kPK:!WrV(|@)!&;bLaj!}rPi' );
define( 'LOGGED_IN_KEY',    'bPG{[],DN$N c]w0*D7kP Yg7aPM<}gJDlSkN/@tvh2Gt%2cR:,Y9653uopuc<Lu' );
define( 'NONCE_KEY',        'MTAZHj$nIsVHeqwd}d2eE1h6FtIl{O]FU(};vE><<L?N8TLPjv>A?<U|YLJU5quN' );
define( 'AUTH_SALT',        'fiDB!=8@M#I,g-~&E#@o5bH,XazY(~UyuOsw?j+5=aV5p?-zJZtu,*R(~$s$jfy ' );
define( 'SECURE_AUTH_SALT', 'jEuzD-&gES|@6j}e]TzJ)z/!7p,{P[(|)P`-fC:4^;xNr=.<Ci*im6i9pSK h70y' );
define( 'LOGGED_IN_SALT',   'hW__YK:)4>]E]P4X+.LqETd&+.~J,ZeqJ9&()u{+FZJX1%bF~q#tEfc[CA)jgU{<' );
define( 'NONCE_SALT',       '^G29xw5+ /y=RUl.<:)9QE6El:MqOp$0>WjFn>mC/[%yu0]eixzlcG/gctN7l9:>' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
